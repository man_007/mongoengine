from models import User
from add import add_document
from update import update_document
from delete import delete_document
from fetch import fetch_documnet

from mongoengine import connect

if __name__=="__main__":
    database_name = input("Input the name of database to connect with: ")
    try:
        connect(db=database_name, host="localhost", port=27017)
        print("You are connected to ",database_name, " database on port 27017")

        while True:
            operation = input("Input the operation that you want to perform(add, update, delete, fetch): ")

            if operation=="add":
                email = input("Email ID: ")
                first_name = input("First name: ")
                last_name  = input("Last name: ")
                age = int(input("Age: "))
                add_document(email=email, first_name=first_name, last_name=last_name, age=age)
            
            elif operation=="update":
                email = input("Enter email id for update: ")
                update_quantity = input("what you want to update(first_name, last_name, age): ")
                updated_value = input("Enter the value: ")
                update_document(email=email, update_quantity=update_quantity, updated_value=updated_value)

            elif operation=="delete":
                email = input("input the email id to find particular document: ")
                delete_document(email=email)
            
            elif operation=="fetch":
                email = input("enter Email Id to fetch the documents: ")
                fetch_documnet(email=email)

            else:
                break
    except:
        print("please enter correct details")



