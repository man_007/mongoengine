# mongoengine

CRUD operations with mongoengine

Requires setup of mongodb and mongoengine.
Setup steps for Windows:
1) Install mongodb in your pc from its official site, 
    Link: https://www.mongodb.com/try/download/community

2) Setup the mongodb and run the server. 

3) Install mongoengine with terminal,
    Code: python -m pip install mongoengine

