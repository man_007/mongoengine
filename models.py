from mongoengine import Document, EmailField, StringField, IntField

class User(Document):
    email = EmailField(required=True)
    first_name = StringField(max_length=100)
    last_name = StringField(max_length=100)
    age = IntField()