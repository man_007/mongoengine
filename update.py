from models import User
from mongoengine import connect

def update_document(email, update_quantity, updated_value):

    if update_quantity=="first_name":
        User.objects(email=email).update(first_name=updated_value)

    elif update_quantity=="last_name":
        User.objects(email=email).update(last_name=updated_value)

    elif update_quantity=="age":
        User.objects(email=email).update(age=int(updated_value))
    else:
        print("Enter Correct value of quantity you want to update.")

